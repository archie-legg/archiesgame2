using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class ammo : MonoBehaviour
{
    public float maxAmmo = 100f;
    public float currentAmmo = 100f;
    public TextMeshProUGUI ammoDisplay;

    // Start is called before the first frame update
    void Start()
    {
        currentAmmo = 100f;

        ammoDisplay = GameObject.Find("Health").GetComponent<TextMeshProUGUI>();
    }

    // Update is called once per frame
    void Update()
    {
        ammoDisplay.text = currentAmmo.ToString();

        if (currentAmmo <= 0)
        {
            currentAmmo = 0;
        }
    }
}
